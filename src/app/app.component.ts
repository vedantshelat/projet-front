import { Component, OnInit } from '@angular/core';
import { Network } from '@ngx-pwa/offline';
import { Observable } from 'rxjs';

import { Store } from '@core/store';
import { ReservationsService } from '@core/reservations';
import { AuthService } from './core/auth/auth.service';

@Component({
  selector: 'app-root',
  template: `
    <div>
      <app-header [isAuthenticated]="isAuthenticated$ | async" [reservationsCount]="reservationsCount$ | async"></app-header>
      <main>
        <router-outlet></router-outlet>
      </main>
    </div>
  `
})
export class AppComponent implements OnInit {

  isAuthenticated$: Observable<boolean>;
  reservationsCount$: Observable<number>;

  // TODO: Inject the Auth service
  constructor(
    protected store: Store,
    protected reservations: ReservationsService,
    protected network: Network,
    private auth: AuthService
  ) {}

  ngOnInit(): void {

    // TODO: Get the authentification status from the Auth service
    this.isAuthenticated$ = this.auth.isAuthenticated;

    // TODO: Get the reservations count from store
    this.reservationsCount$ = this.store.select('reservationsCount');

  }

}
