import { Reservation } from '@core/reservations';

/**
 * Full description of the state object
 */
export interface State {
  reservations: Reservation[];
  reservationsCount: number;
}

/**
 * Default state values that will be loaded on app initialization.
 *
 * **Warning**: there are 2 different types of state properties:
 *
 * 1. some properties have a static default value
 * (eg. an array of products in cart that can be set as an empty array for initialization)
 * => for them, just set a default value here.
 *
 * 2. some properties need a runtime check to determine their initial value
 * (eg. the authentification status needs to check in local storage first)
 * => for them, **do *not* set a default value here, but be sure to have a *core* service**
 * (ie. injected in the `AppComponent`) **where you initialize the value**
 * (ie. in the service's constructor)
 * (otherwise when listening to this property, the Observable could hang on forever).
 *
 */
export const stateDefaults: Partial<State> = {
  reservationsCount: 0,
};
