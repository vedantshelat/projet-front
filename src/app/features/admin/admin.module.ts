import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin.component';
import { AddPage } from './add/add.page';
import { DeletePage } from './delete/delete.page';


@NgModule({
  declarations: [AdminComponent, AddPage, DeletePage],
  imports: [
    CommonModule,
    AdminRoutingModule
  ]
})
export class AdminModule { }
