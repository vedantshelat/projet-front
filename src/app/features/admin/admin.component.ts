import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-admin',
  template: `
    <nav>Menu admin</nav>
    <router-outlet></router-outlet>
  `,
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
