import { Component, ChangeDetectionStrategy, Input } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

import { Movie } from '../../services/movie';

@Component({
  selector: 'app-movie-details',
  template: `
    <article *ngIf="movie">
      <mat-card>
        <iframe [src]="videoSrc" mat-card-image></iframe>
        <mat-card-title>{{ movie.title }}</mat-card-title>
        <mat-card-content>
          <p><ng-container i18n="@@movieReleaseDate">Date de sortie</ng-container> : {{ movie.releasedDate | date }}</p>
          <p>{{ movie.summary }}</p>
        </mat-card-content>
      </mat-card>
    </article>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MovieDetailsComponent {

  @Input() movie: Readonly<Movie> | null;

  get videoSrc(): SafeResourceUrl | string {
    return this.movie ? this.sanitizer.bypassSecurityTrustResourceUrl(this.movie.videoSrc) : '';
  }

  constructor(private sanitizer: DomSanitizer) { }

}
