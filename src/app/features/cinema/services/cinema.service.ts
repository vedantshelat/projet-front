import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, pluck } from 'rxjs/operators';
import { catchOffline } from '@ngx-pwa/offline';

import { environment } from '@core/environment';
import { APIData } from '@core/api';
import { Slide as SlideshowSlide } from '@ui/slideshow';

import { Movie } from './movie';
import { Theater } from './theater';
import { Slide } from './slide';

@Injectable({
  providedIn: 'root'
})
export class CinemaService {

  constructor(private http: HttpClient) {}

  getMovies(): Observable<Movie[]> {

    return this.http.get<APIData<Movie[]>>(`${environment.apiUrl}/api/cinema/movies`).pipe(
      pluck('data'),
      map((movies) => movies.map((movie) => ({
        ...movie,
        imgSrc: `${environment.apiUrl}${movie.imgSrc}`,
      }))),
      catchOffline(),
    );

  }

  getMovie(id: string | number): Observable<Movie> {

    return this.http.get<APIData<Movie>>(`${environment.apiUrl}/api/cinema/movie/${id}`).pipe(
      pluck('data'),
      map((movie) => ({
        ...movie,
        imgSrc: `${environment.apiUrl}${movie.imgSrc}`,
      })),
      map(this.groupSchedules('theater')),
      catchOffline(),
    );

  }

  getTheaters(): Observable<Theater[]> {

    return this.http.get<APIData<Theater[]>>(`${environment.apiUrl}/api/cinema/theaters`).pipe(
      pluck('data'),
      map((theaters) => theaters.map((theater) => ({
        ...theater,
        logoSrc: `${environment.apiUrl}${theater.logoSrc}`,
      }))),
      catchOffline(),
    );

  }

  getTheater(id: string | number): Observable<Theater> {

    return this.http.get<APIData<Theater>>(`${environment.apiUrl}/api/cinema/theater/${id}`).pipe(
      pluck('data'),
      map((theater) => ({
        ...theater,
        logoSrc: `${environment.apiUrl}${theater.logoSrc}`,
      })),
      map(this.groupSchedules('movie')),
      catchOffline(),
    );

  }

  getSlides(): Observable<SlideshowSlide[]> {

    return this.http.get<APIData<Slide[]>>(`${environment.apiUrl}/api/cinema/slides`).pipe(
      pluck('data'),
      map((slides) => slides.map((slide) => ({
        ...slide,
        link: `../movie/${slide.movieId}`,
        imgSrc: `${environment.apiUrl}${slide.imgSrc}`,
        imgSrcFull: `${environment.apiUrl}${slide.imgSrcFull}`,
      }))),
    );

  }

  private groupSchedules(groupBy: 'theater'): (movie: Movie) => Movie;
  private groupSchedules(groupBy: 'movie'): (theater: Theater) => Theater;
  private groupSchedules<T extends Movie | Theater>(groupBy: 'movie' | 'theater'): (movieOrTheater: T) => T {

    return (movieOrTheater: T) => {

      const rawSchedules = movieOrTheater.schedules || [];

      const schedulesGroups = Array.from(new Set<number>(rawSchedules
        .map((schedule) => schedule[groupBy].id)))
        .map((id) => rawSchedules.filter((schedule) => schedule[groupBy].id === id));

      return Object.assign({}, movieOrTheater, { schedulesGroups });

    };

  }

}
