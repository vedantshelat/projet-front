import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Observable } from 'rxjs';

import { Theater } from '../../services/theater';
import { CinemaService } from '../../services/cinema.service';

@Component({
  template: `
    <div>
      <app-theaters-list *ngIf="theaters$ | async as theaters; else loading" [theaters]="theaters"></app-theaters-list>
      <ng-template #loading>
        <div class="center"><mat-progress-spinner mode="indeterminate"></mat-progress-spinner></div>
      </ng-template>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TheatersPage implements OnInit {

  theaters$: Observable<Theater[]>;

  constructor(private cinema: CinemaService) { }

  ngOnInit(): void {

    this.theaters$ = this.cinema.getTheaters();

  }

}
