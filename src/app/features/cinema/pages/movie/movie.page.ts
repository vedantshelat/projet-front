import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';

import { Movie } from '../../services/movie';
import { CinemaService } from '../../services/cinema.service';

@Component({
  template: `
    <div>
      <div *ngIf="movie$ | async as movie; else loading">
        <app-movie-details [movie]="movie"></app-movie-details>
        <app-movie-schedules [schedulesGroups]="movie?.schedulesGroups || []"></app-movie-schedules>
      </div>
      <ng-template #loading>
        <div class="center"><mat-progress-spinner mode="indeterminate"></mat-progress-spinner></div>
      </ng-template>
    </div>
  `
})
export class MoviePage implements OnInit {

  movie$: Observable<Movie>;

  constructor(private cinema: CinemaService, private route: ActivatedRoute) { }

  ngOnInit(): void {

    // TODO: Upgrade to dynamic parameter
    // TODO: Use async pipe
    // TODO: Manage offline errors
    // const id = this.route.snapshot.paramMap.get('id') || '1';

    this.movie$ = this.route.paramMap.pipe(
      map((paramMap) => paramMap.get('id') || '1'),
      switchMap((id) => this.cinema.getMovie(id)),
    );
  }

}
