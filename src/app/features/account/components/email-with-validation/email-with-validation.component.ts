import { Component, Input, ChangeDetectionStrategy, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Observable, of } from 'rxjs';
import { map, catchError, switchMap, first, tap } from 'rxjs/operators';

@Component({
  selector: 'app-email-with-validation',
  template: `
    <!-- TODO: Add form binding -->
    <div [formGroup]="form">
      <mat-form-field>
        <!-- TODO: Add form control name -->
        <input type="email" matInput [formControlName]="name" required autocomplete="email" placeholder="Votre adresse e-mail" i18n-placeholder="@@reactiveEmail">
        <mat-error *ngIf="emailMissing" i18n="@@reactiveEmailMissing">L'e-mail est obligatoire</mat-error>
      </mat-form-field>
    </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EmailWithValidationComponent implements OnInit {

  @Input() form: FormGroup;
  @Input() name = 'email';
  @Input() api: (value: string) => Observable<boolean>;

  get control(): FormControl {
    return this.form.get(this.name) as FormControl;
  }

  get emailMissing(): boolean {

    return this.control.hasError('required') && this.control.dirty;

  }

  constructor(private changeDetector: ChangeDetectorRef) {}

  ngOnInit(): void {

    // TODO: Set async validator
    // TODO: Check if there is a value, otherwise observable of null
    // TODO: Catch error
    // TODO: Manual complete with first()
    // TODO: markForCheck()


  }

}
