import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { of, asyncScheduler, Observable, scheduled } from 'rxjs';
import { tap, map } from 'rxjs/operators';

import { environment } from '@core/environment';
import { APIData } from '@core/api';

import { Token } from './token';
import { AuthService } from '@core/auth';

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  constructor(private http: HttpClient, private auth: AuthService) {}

  login(body: { email: string; password: string }): Observable<APIData<Token>> {

    return this.http.post<APIData<Token>>(`${environment.apiUrl}/api/account/login`, body).pipe(tap(({ data, error }) => {
      if (!error) {
        // TODO: Use the Auth service to login
        this.auth.authenticate(data.token);
      }
    }));

  }

  logout(): Observable<boolean> {

    return scheduled(of(true), asyncScheduler).pipe(tap(() => {
      // TODO: Use the Auth service to logout
      this.auth.deauthenticate();
    }));

  }

  register(body: { email: string; password: string | { password1: string; password2: string; }; }): Observable<APIData<boolean>> {

    return this.http.post<APIData<boolean>>(`${environment.apiUrl}/api/account/register`, body);

  }

  isAvailable(email: string): Observable<boolean> {

    return this.http.get<APIData<boolean>>(`${environment.apiUrl}/api/account/available/${email}`).pipe(map((response) => !response.error));

  }

}
