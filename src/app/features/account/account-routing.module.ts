import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OnlineGuard } from '@ngx-pwa/offline';

import { RegisterPage } from './pages/register/register.page';
import { LoginPage } from './pages/login/login.page';
import { LogoutPage } from './pages/logout/logout.page';
import { ProfilePage } from './pages/profile/profile.page';
import { ReactivePage } from './pages/reactive/reactive.page';
import { AuthGuardGuard } from '@core/auth';

const routes: Routes = [
  { path: 'register', component: RegisterPage, canActivate: [OnlineGuard] },
  { path: 'reactive', component: ReactivePage, canActivate: [OnlineGuard] },
  { path: 'login', component: LoginPage, canActivate: [OnlineGuard] },
  { path: 'logout', component: LogoutPage },
  // TODO: Add guard
  { path: 'profile', component: ProfilePage, canActivate: [AuthGuardGuard] },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccountRoutingModule {}
