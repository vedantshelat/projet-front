import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Observable, of, Subscription } from 'rxjs';
import { switchMap, debounceTime, filter, catchError } from 'rxjs/operators';

import { AccountService } from '../../services/account.service';
import { AutocompleteService } from '../../services/autocomplete.service';

@Component({
  template: `
    <mat-card>
      <!-- TODO: Listen ngSubmit event -->
      <!-- TODO: Add a template reference to the form -->
      <form method="post" (ngSubmit)="register()" #registerForm="ngForm">
        <h1 i18n="@@registerTitle">Inscription</h1>
        <p i18n="@@registerWarning">Attention : il s'agit d'une app de test. E-mail et mot de passe seront visibles en clair par n'importe qui.</p>
        <ul *ngIf="errors.length">
          <li *ngFor="let error of errors">{{ error }}</li>
        </ul>
        <mat-form-field>
          <!-- TODO: Add 2-way binding and control validation -->
          <input type="email" [(ngModel)]="form.email" #emailControl="ngModel" name="email" matInput required autocomplete="email" placeholder="Votre adresse e-mail" i18n-placeholder="@@registerEmail">
          <!-- TODO: Add error feedback for user on empty email (i18n id: @@registerEmailMissing) -->
          <!-- TODO(i18n): @@registerEmailMissing -->
          <p *ngIf="emailControl.invalid && emailControl.touched">L'email est obligatoire</p>
        </mat-form-field>
        <mat-form-field>
          <!-- TODO: Add 2-way binding and control validation -->
          <input type="password" [(ngModel)]="form.password" name="password" matInput required placeholder="Votre mot de passe" i18n-placeholder="@@registerPassword">
          <mat-error i18n="@@registerPasswordMissing">Le mot de passe est obligatoire</mat-error>
        </mat-form-field>
        <div>
          <p i18n="@@registerCard">J'ai une carte :</p>
          <mat-radio-group name="card">
            <mat-radio-button value="" checked i18n="@@registerCardNo">Non</mat-radio-button>
            <mat-radio-button value="ugc">UGC</mat-radio-button>
            <mat-radio-button value="gaumont">Gaumont</mat-radio-button>
          </mat-radio-group>
        </div>
        <mat-form-field>
          <mat-select name="category" placeholder="Genre de film préféré" i18n-placeholder="@@registerGenre">
            <mat-option value="" i18n="@@registerGenreNone">Non spécifié</mat-option>
            <mat-option value="action" i18n="@@registerGenreAction">Action</mat-option>
            <mat-option value="comedy" i18n="@@registerGenreComedy">Comédie</mat-option>
            <mat-option value="drama" i18n="@@registerGenreDrama">Drame</mat-option>
            <mat-option value="horror" i18n="@@registerGenreHorror">Horreur</mat-option>
          </mat-select>
        </mat-form-field>
        <mat-form-field>
          <textarea name="profile" matInput cdkTextareaAutosize placeholder="A propos de vous" i18n-placeholder="@@registerAboutYou"></textarea>
        </mat-form-field>
        <mat-form-field>
          <input type="text" name="city" [formControl]="cityControl" [matAutocomplete]="cityAuto" matInput placeholder="Votre ville" i18n-placeholder="@@registerCity">
        </mat-form-field>
        <mat-autocomplete #cityAuto>
          <!-- TODO: Add <mat-option> -->
            <mat-option *ngFor="let suggestion of citySuggestions" [value]="suggestion">{{suggestion}}</mat-option>
        </mat-autocomplete>

        <div>
          <mat-checkbox name="conditions" required>
            <ng-container i18n="@@registerConditions">J'accepte les conditions d'utilisation</ng-container>. *
          </mat-checkbox>
        </div>
        <!-- TODO: Disable submit button until valid inputs -->
        <button [disabled]="registerForm.invalid" type="submit" mat-raised-button color="accent" i18n="@@registerSubmit">Valider l'inscription</button>
        <p class="center"><a routerLink="../login" i18n="@@registerExistingAccount">Déjà inscrit/e ? Authentifiez-vous.</a></p>
      </form>
    </mat-card>
  `,
  styleUrls: ['./register.page.css']
})
export class RegisterPage implements OnInit, OnDestroy {

  form = {
    email: '',
    password: ''
  };
  cityControl = new FormControl('');
  errors: string[] = [];
  citySuggestions: string[] = [];
  citySubscription: Subscription;
  constructor(
    private account: AccountService,
    private autocomplete: AutocompleteService,
    private router: Router,
    private route: ActivatedRoute,
    private snackBar: MatSnackBar) {}

  ngOnInit(): void {

    // TODO: Autocompletion with RxJS
    this.citySubscription = (this.cityControl.valueChanges as Observable<string>).pipe(
      filter((value) => value.length > 2),
      debounceTime(500),
      switchMap((value) => this.autocomplete.getCitySuggestions(value)),
      catchError(() => of([])),
    ).subscribe((res) => {
      this.citySuggestions = res;
    });
  }

  ngOnDestroy(): void {

    // TODO: Avoid memory leak
    this.citySubscription.unsubscribe();
  }

  register(): void {

    const loading = this.snackBar.open($localize`:@@registerInProgress:Inscription en cours...`);

    this.account.register(this.form).subscribe({
      next: ({ error }) => {

        loading.dismiss();

        if (!error) {

          this.snackBar.open($localize`:@@registerSuccess:Inscription réussie`, $localize`:@@ok:OK`, { duration: 2000 });

          this.router.navigate(['../login'], { relativeTo: this.route, queryParamsHandling: 'preserve' });

        } else {
          this.errors = error.errors || [error.message];
        }

      },
      error: () => {

        loading.dismiss();

        this.errors = [$localize`:@@noInternet:Pas de connexion Internet.`];

      },
    });

  }

}
