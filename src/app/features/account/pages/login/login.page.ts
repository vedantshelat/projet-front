import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';

import { AccountService } from '../../services/account.service';

@Component({
  template: `
    <mat-card>
      <!-- TODO: Listen to submit event -->
      <form method="post" (ngSubmit)="login()" #loginForm="ngForm">
        <h1 i18n="@@loginTitle">Authentification</h1>
        <ul *ngIf="errors.length">
          <li *ngFor="let error of errors">{{ error }}</li>
        </ul>
        <mat-form-field>
          <!-- TODO: Add a binding on the email field -->
          <input type="email" [(ngModel)]="form.email" name="email" matInput required autocomplete="email" placeholder="Votre adresse e-mail" i18n-placeholder="@@loginEmail">
          <mat-error i18n="@@loginEmailMissing">L'e-mail est obligatoire</mat-error>
        </mat-form-field>
        <mat-form-field>
          <!-- TODO: Add a binding on the password field -->
          <input type="password" [(ngModel)]="form.password" name="password" matInput required autocomplete="off" placeholder="Votre mot de passe" i18n-placeholder="@@loginPassword">
          <mat-error i18n="@@loginPasswordMissing">Le mot de passe est obligatoire</mat-error>
        </mat-form-field>
        <button [disabled]="loginForm.invalid" type="submit" mat-raised-button color="accent" i18n="@@loginSubmit">S'authentifier</button>
        <p class="center"><a routerLink="../register" i18n="@@loginNoAccount">Pas encore inscrit/e ? Créez un compte.</a></p>
      </form>
    </mat-card>
  `
})
export class LoginPage {

  form = {
    email: '',
    password: ''
  };
  errors: string[] = [];

  constructor(private account: AccountService, private router: Router, private route: ActivatedRoute, private snackBar: MatSnackBar) {}

  login(): void {

    const loading = this.snackBar.open($localize`:@@loginInProgress:Authentification en cours...`);

    this.account.login(this.form).subscribe({
      next: ({ error }) => {

        loading.dismiss();

        if (!error) {

          this.snackBar.open($localize`:@@loginSuccess:Authentification réussie`, $localize`:@@ok:OK`, { duration: 2000 });

          // TODO: Préserver les paramètres de réservation
          this.router.navigate(['../profile'], { relativeTo: this.route });

        } else {
          this.errors = error.errors || [error.message];
        }

      },
      error: () => {

        loading.dismiss();

        this.errors = [$localize`:@@noInternet:Pas de connexion Internet`];

      },
    });

  }

}

