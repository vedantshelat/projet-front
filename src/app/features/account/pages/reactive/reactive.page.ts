import { Component } from '@angular/core';
import { FormBuilder, Validators, FormGroup, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';

import { AccountService } from '../../services/account.service';
import { AutocompleteService } from '../../services/autocomplete.service';

@Component({
  template: `
    <mat-card>
      <!-- TODO: Add a reactive binding to the form -->
      <form method="post" (ngSubmit)="register()" [formGroup]="form">
        <h1 i18n="@@reactiveTitle">Inscription</h1>
        <p i18n="@@reactiveWarning">Attention : il s'agit d'une app de test. E-mail et mot de passe seront visibles en clair par n'importe qui.</p>
        <!-- TODO: Add email and password components with form binding -->
        <app-email-with-validation [form]="form"></app-email-with-validation>
        <app-password-with-confirmation [form]="form"></app-password-with-confirmation>
        <app-errors [errors]="errors"></app-errors>
        <button type="submit" mat-raised-button color="accent" i18n="@@reactiveSubmit">Valider l'inscription</button>
        <p class="center"><a routerLink="../login" i18n="@@reactiveExistingAccount">Déjà inscrit/e ? Authentifiez-vous.</a></p>
      </form>
    </mat-card>
  `
})
export class ReactivePage {

  // TODO: Form builder
  form = new FormGroup({
    email: new FormControl('', Validators.required),
    password: new FormGroup({
      password1: new FormControl('', Validators.required),
      password2: new FormControl('')
    }),
  });

  errors: string[] = [];

  constructor(
    private account: AccountService,
    private autocomplete: AutocompleteService,
    private router: Router,
    private route: ActivatedRoute,
    private snackBar: MatSnackBar,
    // TODO: Inject FormBuilder service
  ) {}

  register(): void {

    const loading = this.snackBar.open($localize`:@@registerInProgress:Inscription en cours...`);

    this.account.register(this.form.value).subscribe({
      next: ({ error }) => {

        loading.dismiss();

        if (!error) {

          this.snackBar.open($localize`:@@registerSuccess:Inscription réussie`, $localize`:@@ok:OK`, { duration: 2000 });

          this.router.navigate(['../login'], { relativeTo: this.route, queryParamsHandling: 'preserve' });

        } else {
          this.errors = error.errors || [error.message];
        }

      },
      error: () => {

        loading.dismiss();

        this.errors = [$localize`:@@noInternet:Pas de connexion Internet`];

      },
    });

  }

}
