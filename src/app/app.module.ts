import { BrowserModule, HammerModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { offlineProviders } from '@ngx-pwa/offline';

import { LayoutModule } from '@ui/layout';

import { environment } from '../environments/environment';
import { AppRoutingModule } from './app-routing.module';
import { OopsModule } from './features/oops/oops.module';
import { AppComponent } from './app.component';
import { AuthInterceptor } from './core/auth/auth.interceptor';
import { ServiceWorkerModule } from '@angular/service-worker';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    HammerModule,
    LayoutModule,
    OopsModule,
    AppRoutingModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
  ],
  bootstrap: [AppComponent],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true},
    offlineProviders({ routeOffline: '/oops/offline', routeUnavailable: '/oops/unavailable' }),
  ]
})
export class AppModule {}
